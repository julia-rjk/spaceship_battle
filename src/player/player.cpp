#include "player.hpp"

void Player::addPlayer(std::string pathImage){

    if (!texture.loadFromFile(pathImage))
        std::cout<<"Erreur";
    sf::Sprite sprite(texture);
	sprite.setPosition(sf::Vector2f (370,490));
	player.push_back(sprite);
}

sf::Sprite Player::getSprite(int i){
	return this->player[i];
}
std::vector<sf::Sprite> Player::getPlayers(){
	return this->player;
}

void Player::setPosition(int x, int y){
	player[0].setPosition(x,y);
}

void Player::move(int i,int x, int y){
	player[i].move(x,y);
}
void Player::setTexture(int i,std::string path){
	if(!texture.loadFromFile(path))
		std::cout<<"Erreur pour la texture";
	player[i].setTexture(texture);
}

sf::Vector2f Player::getPosition(int i){
	return player[i].getPosition();
}

bool Player::canMoveLeft(int i){
	if(player[i].getPosition().x <0){
		return false;
	}
	return true;
}
bool Player::canMoveRight(int i, sf::Vector2u windowSize){
	if(player[i].getPosition().x >= windowSize.x -100){
		return false;
	}
	return true;
}
bool Player::canMoveUp(int i,sf::Vector2u windowSize){
	if(player[i].getPosition().y < windowSize.y-150){
		return false;
	}
	return true;
}
bool Player::canMoveDown(int i,sf::Vector2u windowSize){
	if(player[i].getPosition().y > windowSize.y-120){
		return false;
	}
	return true;
}

void Player::setScale(float f1, float f2){
		player[0].setScale(f1,f2);
}

