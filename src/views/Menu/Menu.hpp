#ifndef Menu_HPP
#define Menu_HPP

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <chrono>
#include <thread>
using namespace std;
using namespace std::this_thread; // sleep_for, sleep_until
using namespace std::chrono;

#include "views/Tutorial/tutorial.hpp"
#include "SelectLevel.hpp"
#include "views/Multiplayer/Multiplayer.hpp"
using namespace std::this_thread; // sleep_for, sleep_until
using namespace std::chrono;
using namespace std;


//Initialise la fenêtre du menu
int createMenu();
//Permet de savoir si la souris se situe sur un sprite
bool isSpriteHover(sf::FloatRect sprite, sf::Vector2f mp);

#endif //Menu_HPP

